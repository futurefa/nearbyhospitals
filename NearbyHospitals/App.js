/**
 * @flow
 */
import React, {Component} from 'react';
import { createDrawerNavigator ,createStackNavigator} from 'react-navigation';
import HospitalsViewContainer from './modules/hospitals/HospitalsViewContainer';
import MaterialIcons from 'react-native-vector-icons/Ionicons';
import {View,Text,TouchableNativeFeedback} from 'react-native';
import { BorderlessButton } from 'react-native-gesture-handler';
import { DrawerActions } from 'react-navigation-drawer';
const DashBoard = createDrawerNavigator(
  {
    Home: {
      screen: HospitalsViewContainer
    },
  }
);

const App = createStackNavigator(
  {
    // For each screen that you can navigate to, create a new entry like this:
    DashBoard: {
      screen: DashBoard,
      // When `ProfileScreen` is loaded by the StackNavigator, it will be given a `navigation` prop.
      path: '/dashboard'
    }
  },
  {
    initialRouteName: 'DashBoard',
    headerMode: 'float',
    navigationOptions: ({navigation}) => ({
      headerStyle: {backgroundColor: '#0d7dac'},
      title: 'Home',
      headerTintColor: 'white',
      headerLeft: 
      <TouchableNativeFeedback 
          onPress={() => navigation.dispatch(DrawerActions.toggleDrawer())}>
          <View
          style={{marginLeft:16}}>
            <MaterialIcons 
              color="#fff" size={24} 
              name="md-menu"/>
          </View>
      </TouchableNativeFeedback>
        
    })
      
  }
);

export default App;
