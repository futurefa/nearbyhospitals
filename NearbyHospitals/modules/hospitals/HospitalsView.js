import React, {Component} from 'react';
import {View,StatusBar} from 'react-native';
import Map from './Map';

export default class HospitalsView extends Component<Props>{
    
    static navigationOptions = {
        title: 'Map'
    };
    
    render(){
        return (
            <View style={{flex:1}}>
                 <StatusBar
                    barStyle="light-content"
                    />
                 <Map/>
            </View>
           
        )
    }
}
