import React, {Component} from 'react';
import {View,Image,Animated,Text,StyleSheet} from 'react-native';
import Swipeable from 'react-native-gesture-handler/Swipeable';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { RectButton } from 'react-native-gesture-handler';
const AnimatedIcon = Animated.createAnimatedComponent(Icon);

export default class ListItem extends Component<Props>{
    renderLeftActions = (progress, dragX) => {
        const trans = dragX.interpolate({
          inputRange: [0, 50, 100, 101],
          outputRange: [-20, 0, 0, 1],
        });
        return (
          <RectButton style={styles.leftAction} onPress={this.close}>
            <Animated.Text
              style={[
                styles.actionText,
                {
                  transform: [{ translateX: trans }],
                },
              ]}>
              Favorite
            </Animated.Text>
          </RectButton>
        );
    };

    renderRightAction = (text, color, x, progress) => {
        const trans = progress.interpolate({
          inputRange: [0, 1],
          outputRange: [x, 0],
        });
        const pressHandler = () => {
          this.close();
          alert(text);
        };
        return (
          <Animated.View style={{ flex: 1, transform: [{ translateX: trans }] }}>
            <RectButton
              style={[styles.rightAction, { backgroundColor: color }]}
              onPress={pressHandler}>
              <Text style={styles.actionText}>{text}</Text>
            </RectButton>
          </Animated.View>
        );
      };
      renderRightActions = progress => (
        <View style={{ width: 192, flexDirection: 'row' }}>
          {this.renderRightAction('More', '#C8C7CD', 192, progress)}
          {this.renderRightAction('Flag', '#ffab00', 128, progress)}
          {this.renderRightAction('More', '#dd2c00', 64, progress)}
        </View>
      );

      updateRef = ref => {
        this._swipeableRow = ref;
      };

      close = () => {
        this._swipeableRow.close();
      };

    render(){
        return (
            <Swipeable
            ref={this.updateRef}
            friction={2}
            overshootRight={true}
            rightThreshold={40}
            renderLeftActions={this.renderLeftActions}
            renderRightActions={this.renderRightActions}
            >
                <View style={styles.container}>
                    <Image 
                        style={styles.logo} 
                        source={{uri:this.props.logo}}
                        resizeMode={'contain'}
                        />
                    <View>
                        <Text style={styles.name}>{this.props.name}</Text>
                        <Text style={styles.name}>{this.props.overview}</Text>
                    </View>
                </View>
            </Swipeable>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flexDirection: 'row',
        flex:1,
        paddingHorizontal: 16,
        paddingVertical: 8,
        borderBottomWidth: StyleSheet.hairlineWidth
    },
    logo:{
        width:80,
        paddingRight:12
    },
    name:{
        color: "#000"
    },
    actionText: {
        color: 'white',
        fontSize: 16,
        backgroundColor: 'transparent',
        padding: 10,
      },
    summaryContainer:{
        flex: 1,
    },
    leftAction: {
        flex: 1,
        maxWidth:"60%",
        justifyContent: 'center',
    },
    actionIcon: {
        width: 30,
        marginHorizontal: 10,
    },
    rightAction: {
        alignItems: 'flex-end',
        backgroundColor: '#dd2c00',
        flex: 1,
        justifyContent: 'center',
    },
})