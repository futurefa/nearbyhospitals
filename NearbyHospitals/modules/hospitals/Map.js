import React, {Component} from 'react';
import {View, StyleSheet,TextInput,Platform,TouchableNativeFeedback} from 'react-native';
import MapView, { AnimatedRegion, Animated } from 'react-native-maps';
import MaterialIcons from 'react-native-vector-icons/Ionicons';
import { BaseButton } from 'react-native-gesture-handler';

const LATITUDE = 37.78825;
const LONGITUDE = -122.4324;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = 0.0421;
export default class List extends Component<Props>{

    constructor(props) {
        super(props);
        this.state = {
            region: new AnimatedRegion({
              latitude: LATITUDE,
              longitude: LONGITUDE,
              latitudeDelta: LATITUDE_DELTA,
              longitudeDelta: LONGITUDE_DELTA,
            }),
        };
    }
      
    onRegionChange = (region) => {
        this.state.region.setValue(region);
    }

    componentDidMount(){
        Platform.OS === 'ios' && navigator.geolocation.requestAuthorization();
    }

    getCurrentLocation = () => {
        navigator.geolocation.getCurrentPosition(info => {
            const newRegion = new AnimatedRegion({
                latitude: info.coords.latitude,
                longitude: info.coords.longitude,
                latitudeDelta: LATITUDE_DELTA,
                longitudeDelta: LONGITUDE_DELTA,
            });
            this.setState({region:newRegion});
        }, (error) => {
            console.warn(error);
        }, 
        {
            timeout: 20000,
            enableHighAccuracy: false
        });
    }

    render(){
        return (
            <View style={{flex:1}}>
                <View style={styles.container}> 
                    <Animated
                        style={styles.map}
                        region={this.state.region}
                        showsUserLocation={true}
                        showsPointsOfInterest={false}
                        onRegionChange={this.onRegionChange}
                    />
                </View>


                <TextInput 
                    autoFocus={true}
                    placeholder={"Where to?"}
                    style={styles.searchbtn}/>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
      ...StyleSheet.absoluteFillObject,
      flex:1,
      backgroundColor: '#fff',
      justifyContent: 'flex-end',
      alignItems: 'center',
    },
    map: {
      ...StyleSheet.absoluteFillObject,
    },
    searchbtn:{
        position:'absolute',
        top: 40,
        height: 40,
        left:40,
        backgroundColor: '#fff',
        borderColor: '#ececec',
        borderWidth: StyleSheet.hairlineWidth,
        paddingHorizontal: 8,
        right:55
    }
});
