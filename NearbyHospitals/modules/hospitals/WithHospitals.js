import React from 'react';
import hospitals from './seed';

export default (WrappedComponent: ReactComponent) => {
  const Wrapper = props => <WrappedComponent {...props} hospitals={hospitals} />;
  return Wrapper;
};
