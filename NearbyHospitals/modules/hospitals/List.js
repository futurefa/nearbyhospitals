import React, {Component} from 'react';
import {FlatList,View, StyleSheet} from 'react-native';
import ListItem from './ListItem';

export default class List extends Component<Props>{
    render(){
        return (
            <View style={styles.container}> 
                <FlatList
                    data={this.props.hospitals}
                    renderItem={({item}) => <ListItem {...item}/>}
                    keyExtractor={item => item.id}
                    />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop:40,
        backgroundColor: '#F5FCFF',
    },
})
