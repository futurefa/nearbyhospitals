## Purpose

The scope of the project
1. Listing all hospitals nearby using user's device locations
2. Allow the user to order a cab from their location to the selected hospital
3. Showing hospitals that runs 24 hrs and potentially their customer service number
4. Allow the user to favorite the hospital

// TODO: Version 1.0.0
// hospitals
// reviews
// Map
// Search

// TODO: Version 2.0.0
// events

// TODO: Version 3.0.0
// Can favorite a hospital
// Can authenticate
// Can review a hospital
// Can set a reminder on event
// Can order an ambulance service or a cab

// TODO: Version 4.0.0
// Can register to attend the event
// Medical Forms

// TODO: Version 5.0.0
// Ministry of Health Info
// Polio Vacination e.t.c

// TODO: Version 6.0.0
// Insurance accepted in this Hospital.

### How to setup
1. Make sure you have node and npm setup on your machine
1. Run `yarn`
1. To run on android device run `react-native run-android`
1. To run on ios device run `react-native run-ios`
